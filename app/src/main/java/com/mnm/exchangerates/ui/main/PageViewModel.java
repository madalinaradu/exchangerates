package com.mnm.exchangerates.ui.main;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.Map;

public class PageViewModel extends ViewModel {

    private MutableLiveData<Map<String, Double>> mIndex = new MutableLiveData<>();
    private LiveData<Map<String, Double>> mText = Transformations.map(mIndex, new Function<Map<String, Double>, Map<String, Double>>() {
        @Override
        public Map<String, Double> apply(Map<String, Double> text) {
            return text;
        }
    });


    public void setText(Map<String, Double> text) {
        mIndex.setValue(text);
    }

    public LiveData<Map<String, Double>> getText() {
        return mText;
    }
    
    private MutableLiveData<Map<String,Map<String, Double>>> mIndex2 = new MutableLiveData<>();
    private LiveData<Map<String,Map<String, Double>>> historic = Transformations.map(mIndex2, new Function<Map<String,Map<String, Double>>, Map<String,Map<String, Double>>>() {
        @Override
        public Map<String,Map<String, Double>> apply(Map<String,Map<String, Double>> text) {
            return text;
        }
    });


    public void setHistoric(Map<String,Map<String, Double>> text) {
        mIndex2.setValue(text);
    }

    public LiveData<Map<String,Map<String, Double>>> getHistoric() {
        return historic;
    }
}