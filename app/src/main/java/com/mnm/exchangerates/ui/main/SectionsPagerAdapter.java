package com.mnm.exchangerates.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mnm.exchangerates.MainActivity;
import com.mnm.exchangerates.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentStateAdapter implements ViewPager.OnPageChangeListener {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3};
    private Context mContext;


    public CharSequence getPageTitle(int position) {

            return  mContext.getString(TAB_TITLES[position]);
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public SectionsPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);


    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int pageNumber) {
        Bundle bundle = new Bundle();
        bundle.putInt("index", pageNumber+1);
       // PlaceholderFragment frgm = (PlaceholderFragment) getItemId(pageNumber);
        //if(frgm.getActivity().getSupportFragmentManager().getFragments().size()>pos)
        //  frgm = (PlaceholderFragment) frgm.getActivity().getSupportFragmentManager().getFragments().get(pos);
        //else
        ;//if(getSupportFragmentManager().getFragments().size()>1)frgm = (PlaceholderFragment) getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size()-1);
      //  frgm.setArguments(bundle);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setChildListener(View parent, View.OnClickListener listener) {
        parent.setOnClickListener(listener);
        if (!(parent instanceof ViewGroup)) {
            return;
        }

        ViewGroup parentGroup = (ViewGroup) parent;
        for (int i = 0; i < parentGroup.getChildCount(); i++) {
            setChildListener(parentGroup.getChildAt(i), listener);
        }
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return (Fragment) PlaceholderFragment.newInstance(position + 1);    }

    @Override
    public int getItemCount() {
        return 3;
    }

    /*private class TapGestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            PlaceholderFragment frgm = null;

            Bundle bundle = new Bundle();
            bundle.putInt("index", pozition+1);
            frgm = (PlaceholderFragment) getItem(pozition);
            //if(frgm.getActivity().getSupportFragmentManager().getFragments().size()>pos)
            //  frgm = (PlaceholderFragment) frgm.getActivity().getSupportFragmentManager().getFragments().get(pos);
            //else
            ;//if(getSupportFragmentManager().getFragments().size()>1)frgm = (PlaceholderFragment) getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size()-1);
            frgm.setArguments(bundle);
            FragmentTransaction transaction = frgm.getActivity().getSupportFragmentManager()
                    .beginTransaction();
            transaction.setReorderingAllowed(false);

            transaction .detach(frgm)
                    .attach(frgm)
                    .commit();
            return false;
        }
    }*/
}