package com.mnm.exchangerates.ui.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.viewpager.widget.ViewPager;

import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.FastLineAndPointRenderer;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.google.android.material.snackbar.Snackbar;
import com.mnm.exchangerates.MainActivity;
import com.mnm.exchangerates.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import javax.xml.transform.Result;

import static android.content.Context.MODE_PRIVATE;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private PageViewModel pageViewModel;
    private static String currency = "USD";

    public static PlaceholderFragment newInstance(int pos) { //have to make final so we can see it inside of onClick()
        PlaceholderFragment frgm = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("index", pos);
        frgm.setArguments(bundle);
        return frgm;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); }
    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        //setSettingsFromBundle(args);
    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_main, null);
        ConstraintLayout root = (ConstraintLayout) view.findViewById(R.id.mainLayout);


        pageViewModel = new ViewModelProvider(PlaceholderFragment.this).get(PageViewModel.class);



        int position = 1;
        if (getArguments() != null)
            position = getArguments().getInt("index");


        pageViewModel.getHistoric().observe(getViewLifecycleOwner(), new Observer<Map<String, Map<String, Double>>>() {
            @Override
            public void onChanged(@Nullable Map<String, Map<String, Double>> s) {
                }
        });

        pageViewModel.getText().observe(getViewLifecycleOwner(), new Observer<Map<String, Double>>() {
            @Override
            public void onChanged(@Nullable Map<String, Double> s) {

                TextView textViewO = new TextView(root.getContext());
                ScrollView sv = new ScrollView(root.getContext());
                sv.setId(R.id.month_grid);
                String text = "";
                for (Map.Entry<String, Double> entry : s.entrySet()) {
                    text += entry.getKey() + " " + entry + "\n";
                }
                ConstraintLayout.LayoutParams arguments = new ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.MATCH_PARENT);
                ((View) sv).setLayoutParams(arguments);
                textViewO.setText(text);
                textViewO.setGravity(Gravity.CENTER_HORIZONTAL);
                textViewO.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                textViewO.setTextColor(Color.parseColor("#bdbdbd"));
                textViewO.setMinHeight(200);
                textViewO.setMinWidth(200);
                sv.addView(textViewO);
                if (null != root.findViewById(R.id.month_grid))
                    ((ConstraintLayout) root).removeView(sv);
                ((ConstraintLayout) root).addView(sv);
            }
        });


        if(getArguments()!=null)
            position = getArguments().getInt("index");
        //if (position == 1) {

        Map<String, Double> text = new HashMap<>();

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String index = "";


                    // load the sqlite-JDBC driver using the current class loader
                    try {
                        Class.forName("org.sqlite.JDBC");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    SQLiteDatabase connection = null;
                    try {
                        // create a database connection
                        connection = PlaceholderFragment.this.getActivity().getApplicationContext().openOrCreateDatabase("sample.db", MODE_PRIVATE, null);
                        Cursor rs1 = connection.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='exchange';", null);
                        if (!rs1.moveToNext())
                            connection.execSQL("create table exchange (name string, currency real)");
                        try {

                            URL url = new URL("https://api.exchangeratesapi.io/latest&base="+PlaceholderFragment.this.currency);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("GET");
                            conn.setRequestProperty("Accept", "application/json");
                            if (conn.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP Error code : "
                                        + conn.getResponseCode());
                            }
                            InputStreamReader in = new InputStreamReader(conn.getInputStream());
                            BufferedReader br = new BufferedReader(in);
                            String output;
                            String jsonString = br.readLine();
                            JSONObject obj = new JSONObject(jsonString);

                            JSONArray keys = obj.getJSONObject("rates").names();

                            JSONArray arr = obj.getJSONObject("rates").toJSONArray(keys);

                            for (int i = 0; i < arr.length(); i++) {
                                connection.execSQL("insert into exchange values ('"
                                        + keys.get(i) + "', "
                                        + arr.get(i) + ")");
                                text.put(keys.get(i).toString()
                                        , new Double(arr.get(i).toString()));


                            }

                            conn.disconnect();

                        } catch (Exception e) {
                        }
                        {
                            Cursor rs = connection.rawQuery("select * from exchange", null);
                            if (rs != null) if (rs.moveToFirst()) {
                                do {
                                    text.put(rs.getString(0), new Double(rs.getDouble(1)));
                                } while (rs.moveToNext());
                            }

                        }
                    } finally {
                        if (connection != null)
                            connection.close();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //}else if((position == 2)) {
        Map<String, Map<String, Double>> text2 = new HashMap<>();

        Thread thread2 = new Thread(new Runnable() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                try {
                    String index = "";


                    // load the sqlite-JDBC driver using the current class loader
                    try {
                        Class.forName("org.sqlite.JDBC");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    SQLiteDatabase connection = null;
                    try {
                        // create a database connection
                        connection = PlaceholderFragment.this.getActivity().getApplicationContext().openOrCreateDatabase("sample.db", MODE_PRIVATE, null);
                        connection.execSQL("drop table IF EXISTS history ");

                        Cursor rs1 = connection.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='history';", null);
                        if (!rs1.moveToNext())
                            connection.execSQL("create table history (date string, name string, currency real)");
                        try {
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar cal = Calendar.getInstance();

                            cal.add(Calendar.DATE, 1);


                            Calendar cal2 = Calendar.getInstance();
                            cal2.add(Calendar.DATE, -10);

                            URL url = new URL(" https://api.exchangeratesapi.io/history?start_at=" + dateFormat.format(cal2.getTime()) + "&end_at=" + dateFormat.format(cal.getTime()) + "&base="+PlaceholderFragment.this.currency);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("GET");
                            conn.setRequestProperty("Accept", "application/json");
                            if (conn.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP Error code : "
                                        + conn.getResponseCode());
                            }
                            InputStreamReader in = new InputStreamReader(conn.getInputStream());
                            BufferedReader br = new BufferedReader(in);
                            String output;
                            String jsonString = br.readLine();
                            JSONObject obj = new JSONObject(jsonString);

                            JSONArray dates = obj.getJSONObject("rates").names();

                            JSONArray arr = obj.getJSONObject("rates").toJSONArray(dates);

                            for (int i = 0; i < arr.length(); i++) {
                                Map<String, Double> mp = new HashMap<>();
                                JSONArray keys = ((JSONObject) arr.get(i)).names();
                                JSONArray currencies = ((JSONObject) arr.get(i)).toJSONArray(keys);

                                for (int j = 0; j < keys.length(); j++) {
                                    /*connection.execSQL("insert into history values ('"
                                            + dates.get(i) + "', '"
                                            + keys.get(j) + "', "
                                            + currencies.get(j) + ")");*/
                                    mp.put(keys.get(j).toString()
                                            // , new Double(arr.get(j).toString()));
                                            , new Double(currencies.get(j).toString()));


                                }
                                text2.put(dates.get(i).toString(), mp);
                            }
                            int index2 = 0;
                            //String[] cur = (String[]) text.keySet().toArray();
                            String[] domainLabels = (String[]) text2.keySet().toArray(new String[text2.keySet().size()]);
                            float[] series1Numbers = new float[domainLabels.length];
                            float[] series2Numbers = new float[domainLabels.length];
                            for (Map.Entry<String, Map<String,Double>> entry : text2.entrySet()) {
                                for (Object entry2 :  entry.getValue().entrySet()) {
                                    if (((Map.Entry<String, Double>) entry2).getKey().equals("EUR"))
                                        series1Numbers[index2] = new Float((((Map.Entry<String, Double>) entry2).getValue()));
                                    if (((Map.Entry<String, Double>) entry2).getKey().equals( "RON"))
                                        series2Numbers[index2] = new Float((((Map.Entry<String, Double>) entry2).getValue()));

                                }
                                index2++;

                            }

                            conn.disconnect();

                        } catch (Exception e) {
                            e.printStackTrace();                            ;
                        }
                        {
                            Cursor rs = connection.rawQuery("select * from history", null);
                            if (rs != null) if (rs.moveToFirst()) {
                                do {
                                    if (text2.containsKey(rs.getString(0)))
                                        text2.get(rs.getString(0)).put(rs.getString(1), new Double(rs.getDouble(2)));
                                    else {
                                        HashMap hm = new HashMap();
                                        hm.put(rs.getString(1), new Double(rs.getDouble(2)));
                                        text2.put(rs.getString(0), hm);
                                    }
                                } while (rs.moveToNext());
                            }

                        }
                    } finally {
                        if (connection != null)
                            connection.close();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        thread2.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (position==3) {
            Set cur =  text.keySet();
            GridLayout ll = new GridLayout(this.getContext());
            ll.setColumnCount(2);
            ll.setRowCount(cur.size());
            LinearLayout.LayoutParams arguments = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            ll.setLayoutParams(arguments);
           // String[] cur = getArguments().getStringArray("cur");
            for (Object c : cur) {

                TextView tv = new TextView(this.getContext());
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                tv.setTextColor(Color.parseColor("#bdbdbd"));

                tv.setText(c.toString());
                ll.addView(tv);

                CheckBox chck = new CheckBox(this.getContext());
                chck.setTag(c.toString());
                chck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                                                        if (isChecked) {
                                                            String tag = buttonView.getTag().toString();
                                                            PlaceholderFragment.this.currency = tag;

                                                            FragmentTransaction transaction = ((AppCompatActivity)unwrap(PlaceholderFragment.this.getContext())).getSupportFragmentManager()
                                                                    .beginTransaction();

                                                            PlaceholderFragment frgm = new PlaceholderFragment();
                                                            if(((AppCompatActivity)unwrap(PlaceholderFragment.this.getContext())).getSupportFragmentManager().getFragments().size()>1)
                                                                frgm =  (PlaceholderFragment) (((AppCompatActivity)unwrap(PlaceholderFragment.this.getContext())).getSupportFragmentManager().getFragments().get(1));
                                                            transaction.replace(R.id.mainLayout, frgm).commitAllowingStateLoss();
                                                            if(((AppCompatActivity)unwrap(PlaceholderFragment.this.getContext())).getSupportFragmentManager().getFragments().size()>2)
                                                                frgm =  (PlaceholderFragment) (((AppCompatActivity)unwrap(PlaceholderFragment.this.getContext())).getSupportFragmentManager().getFragments().get(2));
                                                            transaction = ((AppCompatActivity)unwrap(PlaceholderFragment.this.getContext())).getSupportFragmentManager()
                                                                    .beginTransaction();
                                                            transaction.replace(R.id.mainLayout, frgm).commitAllowingStateLoss();

                                                        }
                                                    }
                                                }


                );
                ll.addView(chck);


            }
            ((ViewGroup) root).addView(ll);
        }



        if(position==2){
            final XYPlot plot = new XYPlot(PlaceholderFragment.this.getContext(), "Variatia EUR RON fata de USD");
            PixelUtils.init(PlaceholderFragment.this.getContext());


            BarRenderer renderer = (BarRenderer) plot.getRenderer(BarRenderer.class);





            plot.setUserDomainOrigin(0);


            plot.setBorderStyle(XYPlot.BorderStyle.NONE, null, null);


            int index = 0;

            int index2 = 0;
            String[] domainLabels = (String[]) text2.keySet().toArray(new String[text2.keySet().size()]);
            float[] series1Numbers1 = new float[domainLabels.length];
            float[] series2Numbers1 = new float[domainLabels.length];
            for (Map.Entry<String, Map<String,Double>> entry : text2.entrySet()) {
                for (Object entry2 :  entry.getValue().entrySet()) {
                    if (((Map.Entry<String, Double>) entry2).getKey().equals("EUR"))
                        series1Numbers1[index2] = new Float((((Map.Entry<String, Double>) entry2).getValue()));
                    if (((Map.Entry<String, Double>) entry2).getKey().equals( "RON"))
                        series2Numbers1[index2] = new Float((((Map.Entry<String, Double>) entry2).getValue()));

                }
                index2++;

            }
            Number[] series1Numbers = convertFloatsToDoubles(series1Numbers1);
            Number[] series2Numbers = convertFloatsToDoubles(series2Numbers1);


            // turn the above arrays into XYSeries':
            // (Y_VALS_ONLY means use the element index as the x value)
            XYSeries series1 = new SimpleXYSeries(
                    Arrays.asList(series1Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "EUR");
            XYSeries series2 = new SimpleXYSeries(
                    Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "RON");

            // create formatters to use for drawing a series using LineAndPointRenderer
            // and configure them from xml:
            LineAndPointFormatter series1Format = new LineAndPointFormatter(Color.RED, Color.GREEN, Color.BLUE, null);


            LineAndPointFormatter series2Format = new LineAndPointFormatter(Color.RED, Color.GREEN, Color.BLUE, null);

            // add an "dash" effect to the series2 line:
            series2Format.getLinePaint().setPathEffect(new DashPathEffect(new float[]{

                    // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20),
                    PixelUtils.dpToPix(15)}, 0));
            series1Format.getLinePaint().setPathEffect(new DashPathEffect(new float[]{

                    // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20),
                    PixelUtils.dpToPix(15)}, 0));

            // just for fun, add some smoothing to the lines:
            // see: http://androidplot.com/smooth-curves-and-androidplot/
            series1Format.setInterpolationParams(
                    new CatmullRomInterpolator.Params(10, CatmullRomInterpolator.Type.Centripetal));

            series2Format.setInterpolationParams(
                    new CatmullRomInterpolator.Params(10, CatmullRomInterpolator.Type.Centripetal));
            plot.setDomainBoundaries(0, 5, BoundaryMode.FIXED);
            plot.setRangeBoundaries(0, 5, BoundaryMode.FIXED);
            //plot.setDomainStep(StepMode.INCREMENT_BY_VAL, 1.0);
            // add a new series' to the xyplot:
            if ((plot != null) && (series1 != null))
                plot.addSeries(series1,
                        new BarFormatter(
                                Color.rgb(0, 0, 100),
                                Color.rgb(100, 0, 100)));
            if ((plot != null) && (series2 != null))
                plot.addSeries(series2, new BarFormatter(
                        Color.rgb(0, 200, 0),
                        Color.rgb(100, 0, 0)));

            if (plot != null)
                plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
                    @Override
                    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                        int i = Math.round(((Number) obj).floatValue());
                        return toAppendTo.append(domainLabels[i]);
                    }

                    @Override
                    public Object parseObject(String source, ParsePosition pos) {
                        return null;
                    }
                });




            ((ViewGroup) root).addView(plot);



        }
            //pageViewModel.setHistoric(text2);


        if(position == 1)
            pageViewModel.setText(text);


        return view;
    }

    private static Activity unwrap(Context context) {
        while (!(context instanceof Activity) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }

        return (Activity) context;
    }
    public static Number[] convertFloatsToDoubles(float[] input)
    {
        Number[] output = new Number[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = input[i];
        }
        return output;
    }
}